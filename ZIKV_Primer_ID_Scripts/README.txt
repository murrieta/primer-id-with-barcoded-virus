PhD research for Reyes Murrieta

�Research reported was supported by the National Institute Of Allergy And Infectious
Diseases of the National Institutes of Health under Award Number F31AI134108. The content is
solely the responsibility of the authors and does not necessarily represent the official views of
the National Institutes of Health.�

"Project summary"

###################
This workflow requires the following dependincies: 
ruby version 2.3.1 or newer https://www.ruby-lang.org/en/downloads/
BBTools https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/installation-guide/
Perl version 5.22.1 or newer  https://www.perl.org/get.html
###################

Primer ID processing was performed by modifying the template concensus sequences (TCS) for single or multiplexed pair-end Primer ID (PID) MiSeq sequencing, TCS.rb scripts found at https://tcs-dr-dept-tcs.cloudapps.unc.edu/

Input = directory of raw sequences of two ends (R1 and R2 fastq files, zipped or unzipped)

Hardcoded paths --> may need to change (PrimerID_BarcodeGenerator_ZIKVBC.sh, Line 50, change path of referance sequence)

###################


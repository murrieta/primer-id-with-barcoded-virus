#/bin/sh
#Date 1/31/19
#Author: Reyes Murrieta


file_base=$1

log=${file_base}.pipeline.log

mkdir ${file_base}
cp ${file_base}_* ${file_base}
cd  ${file_base}

# Hardcoded paths --> may need to change
#Run from directory that has raw fastq.gz data. 
# bracket to redirect all stdout output to log file
{

   echo "***********************************"
   echo "begin processing sample: $file_base"
   date


ruby /data/ebel_lab/Reyes/Scripts/TCS_ebel_ZIKVBC.rb .

#change file type from .txt to .fasta for TCS_ebel_ZIKVBC.rb output

cp ZIKVBC/consensus/r1.txt ${file_base}_read1.fasta
cp ZIKVBC/consensus/r2.txt ${file_base}_read2.fasta

#zip fasta file_base

gzip *.fasta

f1=${file_base}_read1.fasta.gz
#BBduk to find reads containing 20 nt upstream flanking sequence from ZIKV barcode

bbduk.sh in=$f1 outm=${file_base}_containing_upstream_flank.fa.gz literal=ATAACATCACCTTGGCAATC k=20 hdist=1 ow=t;

f1=${file_base}_containing_upstream_flank.fa.gz

#BBduk to find reads containing 20 nt downstream flanking sequence from ZIKV barcode

bbduk.sh in=$f1 outm=${file_base}_containing_barcode.fa.gz literal=CGGGGCACACTGCTTGTGGC k=20 hdist=1 ow=t;

f1=${file_base}_containing_barcode.fa.gz

#barcode reference sequence for mapping.

barcode1=/data/ebel_lab/Reyes/reference_files/ZIKVic_Barcode.fasta

#map to reference sequence to orient reads.
bbmap.sh in=$f1 outm=${file_base}_mapped.sam ref=$barcode1 ow=t;

#reformat plus and minus strand SAM files to FASTQ

f1=${file_base}_mapped.sam

reformat.sh in=$f1 out=${file_base}_mapped.fastq.gz

#trim reads to only contain barcodes

#remove 20 nt upstream flanking sequence from oriented reads

f1=${file_base}_mapped.fastq.gz

bbduk.sh in=$f1 out=${file_base}_upstream_flank_removed.fastq.gz literal=ATAACATCACCTTGGCAATC ktrim=l rcomp=f k=20 hdist=1 ow=t;

#remove the 20 nt downstream flanking sequence from oriented reads and size select for 24 nt 

f1=${file_base}_upstream_flank_removed.fastq.gz

bbduk.sh in=$f1 out=${file_base}_barcodes.fastq.gz literal=CGGGGCACACTGCTTGTGGC ktrim=r rcomp=f k=20 hdist=1 minlength=24 maxlength=24 ow=t;

f1=${file_base}_barcodes.fastq.gz

#conver fastq to fasta

gunzip $f1

f1=${file_base}_barcodes.fastq

#Convert to fasta.

sed -n '1~4s/^@/>/p;2~4p' $f1 > ${file_base}_barcodes.fasta

#remove headers from fasta file for script input.

f1=${file_base}_barcodes.fasta

grep -v '^>' $f1 > ${file_base}_barcodes.txt

#clean up directory

rm ${file_base}_containing_upstream_flank.fa.gz
rm ${file_base}_containing_barcode.fa.gz
rm ${file_base}_mapped.sam
rm ${file_base}_barcodes.fastq
rm ${file_base}_mapped.fastq.gz
rm ${file_base}_read1.fasta.gz
rm ${file_base}_read2.fasta.gz
rm ${file_base}_upstream_flank_removed.fastq.gz
gzip ${file_base}_barcodes.fasta

#get line counts
perl /data/ebel_lab/Reyes/Scripts/line_counts ${file_base}_barcodes.txt > ${file_base}_barcode_counts.txt
#run stats
perl /data/ebel_lab/Reyes/Scripts/make_column_matrix ${file_base}_barcodes.txt > ${file_base}_barcode_matrix.txt -s -a -h

#

} 2>&1  | tee -a $log  # output tee'd to a logfile


